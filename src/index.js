import observe from './observe.js';//辅助判别
import Watcher from './Watcher.js';
// defineReactive的作用时形成闭包，把临时边练封装起来
var obj = {
    a: {
        m: {
            n: 5
        }
    },
    b: 10,
    c: {
        d: {
            e: {
                f: {
                    r:9999
                }
            }
        }
    },
    g: [22, 33, 44, 55]
};


observe(obj);
new Watcher(obj, 'a.m.n', (val) => {
    console.log('★我是watcher，我在监控a.m.n', val);
});
obj.a.m.n = 88;
obj.c.d.e.f.r = 2;
obj.g.push(66);
console.log(obj);
